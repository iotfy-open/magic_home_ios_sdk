Pod::Spec.new do |spec|

  spec.name                = "MagicSDK"
  spec.version             = "3.0"
  spec.summary             = "Magic Home iOS SDK"
  spec.description         = "Magic Home iOS SDK"
  spec.homepage            = "http://www.iotfy.com/"
  spec.license             = "MIT"
  spec.author              = "Iotfy"
  spec.platform            = :ios, "13.0"
  spec.source              = { :git => "https://bitbucket.org/iotfy-open/magic_home_ios_sdk.git", :tag => 'v3.0' }

  spec.ios.deployment_target = '13.0'
  
  spec.ios.vendored_frameworks = 'MagicSDK.xcframework'
  spec.preserve_paths = 'MagicSDK.xcframework'

  spec.dependency 'BlueSocket'
  spec.dependency 'SwiftyJSON'
  spec.dependency 'CryptoSwift', '~> 1.0'
  spec.dependency 'AWSIoT', '2.26.1'
  spec.dependency 'TrueTime'
  spec.swift_version = "5.0"

end

