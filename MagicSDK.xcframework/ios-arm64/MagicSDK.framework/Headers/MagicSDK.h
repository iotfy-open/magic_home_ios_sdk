//
//  MagicSDK.h
//  MagicSDK
//
//  Created by iotfydev on 29/04/21.
//

#import <Foundation/Foundation.h>

#import "ESPTouchResult.h"
#import "ESPTouchDelegate.h"
#import "ESPTouchTask.h"
#import "ESP_NetUtil.h"
#import <CommonCrypto/CommonCrypto.h>

//! Project version number for MagicSDK.
FOUNDATION_EXPORT double MagicSDKVersionNumber;

//! Project version string for MagicSDK.
FOUNDATION_EXPORT const unsigned char MagicSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MagicSDK/PublicHeader.h>


